<?php

function getContent($json){
    $d = json_decode($json,true);
    if(!is_array($d)){
        $d = json_decode($d,true);
    }
    return $d ?? [];
}
