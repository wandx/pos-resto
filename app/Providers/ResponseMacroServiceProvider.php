<?php

namespace App\Providers;

use Illuminate\Http\JsonResponse;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        ResponseFactory::macro("success",function($data=null){
            $arr = [
                "success" => true,
                "status" => 200,
                "message" => "OK"
            ];

            if($data != null){
                $arr["data"]= $data;
            }
            return response()->json($arr,200);
        });

        ResponseFactory::macro('error', function ($message = "Bad Request", $status = 400) {
            return response()->json([
                'success'  => false,
                "status" => $status,
                'error' => $message,
            ], $status);
        });

        ResponseFactory::macro("error_validation",function($messages){
            return response()->json([
                'success' => false,
                'status' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
                'errors' => $messages
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(){}
}
