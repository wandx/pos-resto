<?php

namespace App\Http\Controllers\Api;

use App\Models\Branch;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileCont extends Controller
{
    public function index(User $user){
        return response()->success([
            "user" => $user->newQuery()->with("branch")->findOrFail(auth("api")->user()->id),
        ]);
    }
}
