<?php

namespace App\Http\Controllers\Api;

use App\Models\Branch;
use App\Http\Controllers\Controller;
use App\Models\Category;

class ProductCont extends Controller
{
    public function getProduct(Branch $branch)
    {
        $branch_id = auth("api")->user()->branch->id;

        $q = $branch->newQuery();
        $q = $q->findOrFail($branch_id);

        $product = $q->products();
        $product->wherePivot("product_has_stock","=",0);
        $product->orWherePivot("stock",">",0);
        $product->with("category");

        if($product->count() == 0){
            return response()->error("Not Found",404);
        }

        return response()->success(["products"=>$product->get()]);
    }

    public function getCategory(Category $category)
    {
        $branch_id = auth("api")->user()->branch->id;
        $categories = $category->newQuery()->whereHas("products",function($product) use ($branch_id){
            $product->whereHas("branches",function($branch) use ($branch_id){
                $branch->where("id",$branch_id);
            });
        });

        if($categories->count() == 0){
            return response()->error("Not Found",404);
        }

        return response()->success(["categories"=>$categories->get()]);
    }
}
