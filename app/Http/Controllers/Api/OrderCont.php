<?php

namespace App\Http\Controllers\Api;

use App\Models\BranchProduct;
use App\Models\Order;
use App\Models\Transaction;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderCont extends Controller
{
    /**
     * @param Request $request
     * @param Transaction $transaction
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function checkout(Request $request,Transaction $transaction)
    {
        $content = getContent($request->getContent());

        $validator = app("validator")->make($content,[
            "customer_name" => "required",
            "cashier_id" => "required|numeric",
            "branch_id" => "required|numeric",
            "order_type" => "required",
            "payment_data" => "required",
            "carts" => "required|array|min:1"
        ]);

        if($validator->fails()){
            return response()->error_validation($validator->messages());
        }

        try{
            DB::beginTransaction();

            $code = "ORD".Carbon::now()->format("dmYHis");

            $store_transaction = $transaction->newQuery()->create([
                "code" => $code,
                "customer_name" => $content["customer_name"],
                "cashier_id" => $content["cashier_id"],
                "branch_id" => $content["branch_id"],
                "order_type" => $content["order_type"],
                "discount" => $content["discount"] ?? 0,
                "is_discount_in_percent" => $content["is_discount_in_percent"] ?? 1,
                "tax" => $content["tax"] ?? 0,
                "grand_total" => $content["grand_total"] ?? 0,
                "payment_data" => (is_array($content["payment_data"]) ? json_encode($content["payment_data"]):$content["payment_data"]) ?? 0,
                "order_status" => $content["status"] ?? "draft",
            ]);

            foreach ($content["carts"] as $cart){
                $store_transaction->orders()->create([
                    "product_id" => $cart["product_id"],
                    "product_name" => $cart["product_name"],
                    "notes" => $cart["notes"] ?? "-",
                    "base_price" => $cart["base_price"] ?? 0,
                    "sell_price" => $cart["sell_price"] ?? 0,
                    "quantity" => $cart["quantity"],
                    "sub_total" => $cart["sub_total"],
                    "parent_id" => $cart["parent_id"] ?? null
                ]);

                $this->reduceStock($cart["product_id"],$content["branch_id"],$cart["quantity"]);
            }

            DB::commit();

            return response()->success();

        }catch (Exception $e){

            DB::rollBack();

            return response()->error($e->getMessage(),$e->getCode() == 0 ? 400 :$e->getCode());
        }
    }

    private function reduceStock($pid,$bid,$qty)
    {
        $branchProduct = new BranchProduct();
        $bp = $branchProduct->newQuery()->where("product_id",$pid)->where("branch_id",$bid)->first();
        $current_stock = $bp->stock - $qty;

        if($bp->product_has_stock == 1){
            if($current_stock >= 0){
                $bp->update(["stock"=>$current_stock]);
                return [
                    "success" => true,
                    "product_id" => $pid
                ];
            }
        }

        return [
            "success" => false,
            "product_id" => $pid
        ];
    }

    public function getOrders($branch_id,Request $request,Transaction $transaction)
    {
        $transaction = $transaction->newQuery();

        $transaction->where(function($q) use ($branch_id,$request){
           $q->where("branch_id",$branch_id);

           if($request->has("status") && $request->get("status") != ""){
               $q->where("order_status",$request->get("status"));
           }
        });

        if($transaction->count() == 0){
            return response()->error("Not Found",404);
        }

        return response()->success(["transactions" => $transaction->with("orders","branch","cashier")->orderBy("created_at","asc")->get()]);
    }

    public function changeStatus(Request $request,Transaction $transaction)
    {
        $content = json_decode($request->getContent(),true) ?? [];

        $id = $content["id"];
        $status = $content["status"];

        $data["order_status"] = $status;

        if(auth("api")->user()->role == "kitchen"){
            $data["kitchen_id"] = auth("api")->user()->id;
        }

        if(auth("api")->user()->role == "cashier"){
            $data["cashier_id"] = auth("api")->user()->id;
        }

        $transaction->newQuery()->find($id)->update($data);

        return response()->success();
    }
}
