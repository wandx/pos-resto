<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthCont extends Controller
{
    public function login(Request $request){
        $content = getContent($request->getContent());
        $validator = app("validator")->make($content,[
            "username" => "required|exists:users,username",
            "password" => "required"
        ]);

        if($validator->fails()){
            return response()->error_validation($validator->messages());
        }

        if(!$token = auth("api")->attempt(["username"=>$content["username"],"password"=>$content["password"]])){
            return response()->error("Credential not match",422);
        }

        return response()->success(["token"=>$token]);
    }

    public function logout(){
        auth("api")->logout();
        return response()->success();
    }
}
