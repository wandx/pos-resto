<?php

namespace App\Http\Controllers\Admin\Product;

use App\Models\Branch;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ramsey\Uuid\Uuid;
use Storage;
use Yajra\DataTables\Facades\DataTables;

class ProductCont extends Controller
{
    public function index(Branch $branch,Category $category)
    {
        $branches = $branch->newQuery()->pluck("title","id");
        $categories = $category->newQuery()->pluck("display_name","id");
        return view("product.index",compact("branches","categories"));
    }

    /**
     * @param Request $request
     * @param Product $product
     * @return mixed
     * @throws \Exception
     */
    public function data(Request $request,Product $product)
    {
        $q = $product->newQuery()->with("category","branches");

        return DataTables::of($q)
                ->filter(function($query) use ($request){
                    // filter by branch
                    if($request->has("branch_id") && $request->get("branch_id") != ""){
                        $query->whereHas("branches",function($branch) use ($request){
                            $branch->where("id",$request->get("branch_id"));
                        });
                    }

                    // filter by category
                    if($request->has("category_id") && $request->get("category_id") != ""){
                        $query->where("category_id",$request->get("category_id"));
                    }

                })
                ->addColumn("action",function($model){
                    $sel = $model->branches->pluck("id")->toArray() ?? [];
                    $bid = implode(",",$sel);


                    $li = "<button
                                class='btn btn-info btn-xs'
                                data-toggle='modal'
                                data-target='#modal-product'
                                data-id='$model->id'
                                data-name='$model->name'
                                data-baseprice='$model->base_price'
                                data-sellprice='$model->sell_price'
                                data-cid='$model->category_id'
                                data-bid='$bid'
                                ><i class='fa fa-pencil'></i></button>";

                    return $li;
                })
                ->addColumn("branches",function($model){
                    $x = $model->branches()->pluck("title")->toArray() ?? [];
                    return implode(",",$x);
                })
                ->make(true);
    }

    /**
     * @param Request $request
     * @param Product $product
     * @return mixed
     * @throws \Exception
     */
    public function store(Request $request,Product $product)
    {
        if($request->has("id") && $request->input("id") != ""){
            $c = $product->newQuery()->findOrFail($request->input("id"));

            if($request->hasFile("img")){
                Storage::delete($c->image_path);
            }
        }

        if($request->hasFile("img")){
            $path = "product/";
            $name = str_random(10).".".$request->file("img")->getClientOriginalExtension();

            $request->file("img")->storeAs($path,$name,"public");
            $request->merge(["image_path"=>$path.$name]);
        }

        $store = $product->newQuery()->updateOrCreate(["id"=>$request->input("id")],$request->except("img","id","_token","branch_ids"));

        $pid = $store->id;

        $p = $product->newQuery()->findOrFail($pid);

        if($request->has("branch_ids")){
            $ids = [];
            foreach ($request->input("branch_ids") as $branch){
                $ids[$branch] = ["product_price"=>$request->input("sell_price"),"bp_id"=>Uuid::uuid4()];
            }
            $p->branches()->sync($ids);
        }

        return back()->withSuccess("Produk berhasil disimpan.");
    }

    public function destroy(){}
}
