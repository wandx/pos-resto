<?php

namespace App\Http\Controllers\Admin\Product;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class CategoryCont extends Controller
{
    public function index(){
        return view("product.category");
    }

    public function data(Category $category){
        return DataTables::of($category->newQuery()->with("products"))
            ->addColumn("action",function($model){
                $has_product = $model->products->count() > 0;
                $class = !$has_product ? "btn-start":"";

                $li = "<button class='btn btn-info btn-xs $class' data-toggle='modal' data-target='#modal-category' data-id='$model->id' data-name='$model->display_name'><i class='fa fa-pencil'></i></button>";
                if(!$has_product){
                    $li .= "<a class='btn btn-danger btn-xs btn-last' href='".route("admin.product.category.destroy",["id"=>$model->id])."' onclick='return confirm(\"Hapus Kategori $model->display_name ?\")'><i class='fa fa-trash'></i></a>";
                }

                return $li;
            })
            ->addColumn("product_count",function($model){
                return $model->products->count() ?? 0;
            })
            ->make(true);
    }

    public function store(Request $request,Category $category){
        $request->merge(["slug"=>Str::slug($request->input("display_name"))]);
        $category->newQuery()->updateOrCreate(["id"=>$request->input("id")],$request->except("id","_token"));
        return back()->withSuccess("Kategori tersimpan.");
    }

    public function destroy($id,Category $category){
        $c = $category->newQuery()->findOrFail($id);

        if($c->products->count() > 0){
            return back()->withErrors(["failed"=>"Gagal menghapus kategori. Kategori memiliki produk."]);
        }

        $c->delete();
        return back()->withSuccess("Kategori telah dihapus.");
    }
}
