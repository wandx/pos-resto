<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginCont extends Controller
{
    public function index(){
        return view("login");
    }

    public function doLogin(Request $request){
        $validator = app("validator")->make($request->all(),[
            "username" => "required",
            "password" => "required"
        ]);

        if($validator->fails()){
            return back()->withErrors($validator->messages())->withInput();
        }

        if(!auth("admin")->attempt($request->only(["username","password"]),$request->filled("remember"))){
            return back()->withErrors(["failed"=>"Kombinasi Email dan password belum benar"])->withInput();
        }

        return redirect("admin/dashboard")->withSuccess("Selamat datang ".auth("admin")->user()->name);
    }

    public function doLogout(){
        auth("admin")->logout();
        return redirect()->route("admin.login")->withSuccess("Anda berhasil logout.");
    }
}
