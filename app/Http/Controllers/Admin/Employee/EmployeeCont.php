<?php

namespace App\Http\Controllers\Admin\Employee;

use App\Http\Requests\Employee\StoreEmployeeRequest;
use App\Http\Requests\Employee\UpdateEmployeeRequest;
use App\Models\Branch;
use App\Models\User;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeeCont extends Controller
{
    public function index(Branch $branch){
        $branches = $branch->newQuery()->pluck("title","id");
        $roles = [
            "cashier"=>"Kasir",
            "waiters" =>"Waiters",
            "kitchen" => "Dapur"
        ];

        return view("employee.index",compact("branches","roles"));
    }

    public function data(Request $request, User $user){
        return Datatables::of($user->newQuery()->with("branch"))
            ->filter(function($query) use ($request){
                if($request->has("branch_id") && $request->get("branch_id") != ""){
                    $query->where("branch_id",$request->get("branch_id"));
                }

                if($request->has("role") && $request->get("role") != ""){
                    $query->where("role",$request->get("role"));
                }
            })
            ->addColumn("action",function($model){
                $li = "<button
                            class='btn btn-info btn-xs btn-start'
                            data-toggle='modal'
                            data-target='#modal-employee-update'
                            data-id='$model->id'
                            data-name='$model->name'
                            data-username='$model->username'
                            data-email='$model->email'
                            data-role='$model->role'
                            data-bid='$model->branch_id'
                            ><i class='fa fa-pencil'></i></button>";

                $li .= "<a class='btn btn-danger btn-xs btn-last' href='".route("admin.employee.destroy",["id"=>$model->id])."' onclick='return confirm(\"Hapus Pegawai $model->name ?\")'><i class='fa fa-trash'></i></a>";
                return $li;
            })
            ->make(true);
    }

    public function store(StoreEmployeeRequest $request,User $user){
        $request->merge(["password"=>bcrypt($request->input("password"))]);
        $user->newQuery()->create($request->except("_token"));
        return back()->withSuccess("Pegawai telah ditambahkan.");
    }

    public function update(UpdateEmployeeRequest $request,User $user){
        if($request->has("password")){
            $request->merge(["password"=>bcrypt($request->input("password"))]);
        }else{
            $request->offsetUnset("password");
        }

        $user->newQuery()->findOrFail($request->input("id"))->update($request->except("_token","id"));
        return back()->withSuccess("Data tersimpan.");
    }

    public function destroy($id,User $user){
        $x = $user->newQuery()->findOrFail($id);
        $x->delete();
        return back()->withSuccess("Pegawai telah di hapus.");
    }
}
