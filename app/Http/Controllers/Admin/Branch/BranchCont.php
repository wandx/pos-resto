<?php

namespace App\Http\Controllers\Admin\Branch;

use App\Models\Branch;
use App\Models\BranchProduct;
use App\Models\Product;
use DataTables;
use Html;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BranchCont extends Controller
{
    public function index(){
        return view("branch.index");
    }

    public function data(Branch $branch){
        return Datatables::of($branch->newQuery()->with("products"))
            ->addColumn("product_count",function($model){
                $count = $model->products->count() ?? 0;

                if($count > 0){
                    $count = Html::link(route("admin.branch.product_list",["branch_id"=>$model->id]),$count);
                }

                return $count;
            })
            ->addColumn("user_count",function($model){
                return $model->users->count() ?? 0;
            })
            ->addColumn("action",function($model){
                $has_product = $model->products->count() > 0 || $model->users->count() > 0? true:false;
                $class = !$has_product ? "btn-start":"";

                $li = "<button class='btn btn-info btn-xs $class' data-toggle='modal' data-target='#modal-branch' data-id='$model->id' data-name='$model->title' data-address='$model->address' data-phone='$model->phone'><i class='fa fa-pencil'></i></button>";
                if(!$has_product){
                    $li .= "<a class='btn btn-danger btn-xs btn-last' href='".route("admin.branch.destroy",["id"=>$model->id])."' onclick='return confirm(\"Hapus Gerai $model->title ?\")'><i class='fa fa-trash'></i></a>";
                }

                return $li;
            })
            ->rawColumns(["action","product_count"])
            ->make(true);
    }

    public function product_list($branch_id, Branch $branch){
        $branch = $branch->newQuery()->findOrFail($branch_id);
        return view("branch.product",compact("branch"));
    }

    public function product_list_data($branch_id,Request $request, Product $product){
        $q = $product->newQuery()->whereHas("branches",function($q) use ($branch_id){
            $q->where("id",$branch_id);
        })->with("category");

        return DataTables::of($q)
            ->filter(function($query) use ($request){
                // filter by branch
                if($request->has("branch_id") && $request->get("branch_id") != ""){
                    $query->whereHas("branches",function($branch) use ($request){
                        $branch->where("id",$request->get("branch_id"));
                    });
                }

                // filter by category
                if($request->has("category_id") && $request->get("category_id") != ""){
                    $query->where("category_id",$request->get("category_id"));
                }

            })
            ->addColumn("in_branch_price",function($model) use ($branch_id){
                return $this->get_branch_price($model->id,$branch_id);
            })
            ->addColumn("product_has_stock",function($model) use ($branch_id){
                return $this->get_product_has_stock($model->id,$branch_id) == 1 ? "YA":"TIDAK";
            })
            ->addColumn("stock_count",function($model) use ($branch_id){
                $stock = "-";
                if($this->get_product_has_stock($model->id,$branch_id) == 1){
                    $stock = $this->get_product_stock($model->id,$branch_id);
                }
                return $stock;
            })
            ->addColumn("action",function($model) use ($branch_id){
                $p = $this->get_branch_price($model->id,$branch_id);
                $id = $this->get_branch_price_id($model->id,$branch_id);
                $with_stock = $this->get_product_has_stock($model->id,$branch_id);
                $stock = $this->get_product_stock($model->id,$branch_id);
                $li = "<button
                                class='btn btn-info btn-xs'
                                data-toggle='modal'
                                data-target='#modal-product'
                                data-id='$id'
                                data-sellprice='$p'
                                data-withstock='$with_stock'
                                data-stock='$stock'
                                ><i class='fa fa-pencil'></i></button>";

                return $li;
            })
            ->make(true);
    }

    public function store(Request $request,Branch $branch){
        $branch->newQuery()->updateOrCreate(["id"=>$request->input("id")],$request->except("_token","id"));
        return back()->withSuccess("Gerai tersimpan.");
    }

    public function destroy($id,Branch $branch){
        $f = $branch->newQuery()->findOrFail($id);

        if($f->products->count() > 0){
            return back()->withErrors(["failed"=>"Gagal menghapus gerai, gerai memiliki produk."]);
        }

        $f->delete();
        return back()->withSuccess("Gerai berhasil dihapus.");
    }

    public function update_product_price(Request $request, BranchProduct $branchProduct){
        $branchProduct->newQuery()->where("bp_id",$request->input("id"))->first()->update(["product_price"=>$request->input("sell_price"),"product_has_stock"=>$request->input("with_stock",0),"stock"=>$request->input("stock",0)]);
        return back()->withSuccess("Data tersimpan.");
    }

    private function get_branch_price($pid,$bid){
        $b = (new BranchProduct())->newQuery()->where("product_id",$pid)->where("branch_id",$bid)->first();
        return $b->product_price;
    }

    private function get_branch_price_id($pid,$bid){
        $b = (new BranchProduct())->newQuery()->where("product_id",$pid)->where("branch_id",$bid)->first();
        return $b->bp_id;
    }

    private function get_product_has_stock($pid,$bid){
        $b = (new BranchProduct())->newQuery()->where("product_id",$pid)->where("branch_id",$bid)->first();
        return $b->product_has_stock;
    }

    private function get_product_stock($pid,$bid){
        $b = (new BranchProduct())->newQuery()->where("product_id",$pid)->where("branch_id",$bid)->first();
        return $b->stock;
    }


}
