<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Product extends Model
{
    protected $guarded = ["id"];
    protected $appends = ["image_url"];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function branches()
    {
        return $this->belongsToMany(Branch::class)->withPivot(["product_price","bp_id","product_has_stock","stock"]);
    }

    public function getImageUrlAttribute()
    {
        $img = asset("img/default.gif");

        if($this->image_path != null && Storage::disk("public")->exists($this->image_path)){
            $img = asset(Storage::disk("public")->url($this->image_path));
        }

        return $img;
    }
}
