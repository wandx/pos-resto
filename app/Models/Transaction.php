<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class Transaction extends Model
{
    protected $guarded = ["id"];
    public $incrementing = false;

    const STATUS_PENDING="pending";
    const STATUS_ONPROGRESS="on_progress";
    const STATUS_DONE="done";
    const STATUS_TAKEN="taken";
    const STATUS_DRAFT="draft";

    const TYPE_DINE_IN="dine_in";
    const TYPE_TAKE_AWAY="take_away";
    const TYPE_GOJEK="gojek";

    protected $casts = [
        "payment_data" => "array"
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function cashier()
    {
        return $this->belongsTo(User::class,"cashier_id");
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    protected static function boot()
    {
        parent::boot();
        self::creating(function($model){
            $model->id = Uuid::uuid4();
        });
    }
}
