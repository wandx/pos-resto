<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BranchProduct extends Model
{
    protected $guarded = ["bp_id"];
    protected $table = "branch_product";
    protected $primaryKey = "bp_id";
    public $incrementing = false;
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo(Product::class,"product_id");
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class,"branch_id");
    }
}
