<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $guarded = ["id"];

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot(["bp_id","product_price","product_has_stock","stock"]);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
