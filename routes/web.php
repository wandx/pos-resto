<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route("admin.login");
});

Route::group(["prefix"=>"admin","namespace"=>"Admin","as"=>"admin."],function(){
    Route::middleware("guest:admin")->get("login","LoginCont@index")->name("login");
    Route::middleware("guest:admin")->post("login","LoginCont@doLogin")->name("login.post");

    Route::group(["middleware"=>"auth:admin"],function(){
        Route::view("dashboard","dashboard")->name("dashboard");
        Route::get("logout","LoginCont@doLogout")->name("logout");

        Route::group(["prefix"=>"product","as"=>"product.","namespace"=>"Product"],function(){
            Route::get("/","ProductCont@index")->name("index");
            Route::post("/","ProductCont@store")->name("store");
            Route::get("data","ProductCont@data")->name("data");

            Route::group(["prefix"=>"category","as"=>"category."],function(){
                Route::get("/","CategoryCont@index")->name("index");
                Route::post("/","CategoryCont@store")->name("store");
                Route::get("data","CategoryCont@data")->name("data");
                Route::get("{id}/destroy","CategoryCont@destroy")->name("destroy");
            });
        });

        Route::group(["prefix"=>"branch","as"=>"branch.","namespace"=>"Branch"],function(){
            Route::get("/","BranchCont@index")->name("index");
            Route::post("/","BranchCont@store")->name("store");
            Route::get("data","BranchCont@data")->name("data");
            Route::get("{id}/destroy","BranchCont@destroy")->name("destroy");

            Route::get("{branch_id}/product","BranchCont@product_list")->name("product_list");
            Route::get("{branch_id}/product/data","BranchCont@product_list_data")->name("product_list.data");
            Route::post("/update_product_price","BranchCont@update_product_price")->name("product_list.update_price");
        });

        Route::group(["prefix"=>"employee","as"=>"employee.","namespace"=>"Employee"],function(){
            Route::get("/","EmployeeCont@index")->name("index");
            Route::get("data","EmployeeCont@data")->name("data");
            Route::post("store","EmployeeCont@store")->name("store");
            Route::post("update","EmployeeCont@update")->name("update");
            Route::get("{id}/destroy","EmployeeCont@destroy")->name("destroy");
        });
    });
});