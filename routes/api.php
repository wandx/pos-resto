<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(["namespace"=>"Api"],function(){
    Route::middleware("guest:api")->post("login","AuthCont@login");
    Route::post("logout","AuthCont@logout");

    Route::group(["middleware"=>"auth:api"],function(){
        Route::get("me","ProfileCont@index");
        Route::get("products","ProductCont@getProduct");
        Route::get("categories","ProductCont@getCategory");

        Route::post("checkout","OrderCont@checkout");
        Route::get("orders/{branch_id}","OrderCont@getOrders");
        Route::post("change_status","OrderCont@changeStatus");
    });
});
