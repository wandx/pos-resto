@extends("master")

@section("page_header","Pegawai")
@section("page_description","Daftar Pegawai")
@push("contents")
    <div class="panel">
        <form class="panel-body" id="f-filter">
            <div class="col-sm-4">
                {!! Form::select("branch_id",$branches,null,["class"=>"form-control","placeholder"=>"Semua Gerai","id"=>"bid-filter"]) !!}
            </div>

            <div class="col-sm-4">
                {!! Form::select("role",$roles,null,["class"=>"form-control","placeholder"=>"Semua Jabatan","id"=>"role-filter"]) !!}
            </div>

            <div class="col-sm-4">
                <button type="submit" class="btn btn-primary">Filter</button>
                &nbsp;
                <button class="btn btn-info" data-toggle="modal" data-target="#modal-employee">Tambah Pegawai</button>
            </div>
        </form>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <table class="table table-hover dataTable" id="dtable">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Jabatan</th>
                        <th>Gerai</th>
                        <th>Created</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
@endpush

@push("contents")
    <!-- Modal -->
    <div id="modal-employee" class="modal fade" role="dialog">
        <form method="post" action="{{ route("admin.employee.store") }}" class="modal-dialog" id="f-employee">
            {!! csrf_field() !!}
            <input type="hidden" name="id">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Pegawai</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {!! Form::label("name","Nama") !!}
                        {!! Form::text("name",null,["class"=>"form-control","required"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label("username","Username") !!}
                        {!! Form::text("username",null,["class"=>"form-control","required"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label("email","Email") !!}
                        {!! Form::email("email",null,["class"=>"form-control","required"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label("password","Password") !!}
                        {!! Form::password("password",["class"=>"form-control","required"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label("role","Jabatan") !!}
                        {!! Form::select("role",$roles,null,["class"=>"form-control","required"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label("branch_id","Gerai") !!}
                        {!! Form::select("branch_id",$branches,null,["class"=>"form-control","required"]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-default">Simpan</button>
                </div>
            </div>

        </form>
    </div>


    <!-- Modal -->
    <div id="modal-employee-update" class="modal fade" role="dialog">
        <form method="post" action="{{ route("admin.employee.update") }}" class="modal-dialog" id="f-employee-update">
            {!! csrf_field() !!}
            <input type="hidden" name="id">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Pegawai</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {!! Form::label("name-update","Nama") !!}
                        {!! Form::text("name",null,["class"=>"form-control","required","id"=>"name-update"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label("username-update","Username") !!}
                        {!! Form::text("username",null,["class"=>"form-control","required","id"=>"username-update"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label("email-update","Email") !!}
                        {!! Form::email("email",null,["class"=>"form-control","required","id"=>"email-update"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label("password-update","Password") !!}
                        {!! Form::password("password",["class"=>"form-control","id"=>"password-update"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label("role-update","Jabatan") !!}
                        {!! Form::select("role",$roles,null,["class"=>"form-control","required","id"=>"role-update"]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label("branch_id-update","Gerai") !!}
                        {!! Form::select("branch_id",$branches,null,["class"=>"form-control","required","id"=>"branch_id-update"]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-default">Simpan</button>
                </div>
            </div>

        </form>
    </div>
@endpush

@push("scripts")
    <script>
        var dtable,dtableElem = $("#dtable");

        $(function(){
            dtable = dtableElem.DataTable({
                processing: true,
                serverSide: true,
                paging: false,
                searching:false,
                info:false,
                ajax: {
                    url: '{!! route('admin.employee.data') !!}',
                    data: function(d){
                        d.branch_id = $("#bid-filter").val();
                        d.role = $("#role-filter").val();
                    }
                },
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'username', name: 'username' },
                    { data: 'role', name: 'role' },
                    { data: 'branch.title', name: 'branch.title',orderable: false,searchable: false },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action',orderable: false,searchable: false }
                ],
                order:[
                    [0,"asc"]
                ],
                columnDefs:[
                    {className:"text-center",targets:[1,2,3,5]},
                    {className:"text-right",targets:[4]}
                ]
            });
        });


        $(document).on("show.bs.modal","#modal-employee",function(e){
            var trig = $(e.relatedTarget),
                name = trig.data("name") || "",
                id = trig.data("id") || "",
                modal = $(this);

            modal.find("input[name=display_name]").val(name);
            modal.find("input[name=id]").val(id);
        });

        $(document).on("show.bs.modal","#modal-employee-update",function(e){
            var trig = $(e.relatedTarget),
                name = trig.data("name") || "",
                id = trig.data("id") || "",
                email = trig.data("email") || "",
                role = trig.data("role") || "",
                branchId = trig.data("bid") || "",
                username = trig.data("username") || "",
                modal = $(this);

            modal.find("input[name=id]").val(id);
            modal.find("input[name=name]").val(name);
            modal.find("input[name=email]").val(email);
            modal.find("select[name=role]").val(role);
            modal.find("select[name=branch_id]").val(branchId);
            modal.find("input[name=username]").val(username);
        });

        $(document).on("submit","#f-filter",function(e){
            e.preventDefault();
            dtable.ajax.reload();
        });


    </script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest(\App\Http\Requests\Employee\StoreEmployeeRequest::class, '#f-employee'); !!}
    {!! JsValidator::formRequest(\App\Http\Requests\Employee\UpdateEmployeeRequest::class, '#f-employee-update'); !!}

@endpush