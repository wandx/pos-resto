<ul class="sidebar-menu" data-widget="tree">
    <!-- Optionally, you can add icons to the links -->
    <li class=""><a href="{{ route("admin.dashboard") }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
    <li class="treeview">
        <a href="#">
            <i class="fa fa-archive"></i> <span>Produk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu" style="display: none;">
            <li><a href="{{ route("admin.product.index") }}"><i class="fa fa-circle-o"></i> Daftar Produk</a></li>
            <li><a href="{{ route("admin.product.category.index") }}"><i class="fa fa-circle-o"></i> Kategori Produk</a></li>
        </ul>
    </li>

    <li class=""><a href="{{ route("admin.employee.index") }}"><i class="fa fa-user-secret"></i> <span>Pegawai</span></a></li>
    <li class=""><a href="{{ route("admin.branch.index") }}"><i class="fa fa-building-o"></i> <span>Gerai</span></a></li>
    {{--<li class=""><a href="{{ route("admin.branch.index") }}"><i class="fa fa-money"></i> <span>Keuangan</span></a></li>--}}

    {{--<li class="treeview">--}}
        {{--<a href="#">--}}
            {{--<i class="fa fa-book"></i> <span>Laporan</span>--}}
            {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
        {{--</a>--}}
        {{--<ul class="treeview-menu" style="display: none;">--}}
            {{--<li><a href="#"><i class="fa fa-circle-o"></i> Berdasarkan Tanggal</a></li>--}}
            {{--<li><a href="#"><i class="fa fa-circle-o"></i> Berdasarkan Jam</a></li>--}}
            {{--<li><a href="#"><i class="fa fa-circle-o"></i> Berdasarkan Jenis Order</a></li>--}}
            {{--<li><a href="#"><i class="fa fa-circle-o"></i> Menu terlaris</a></li>--}}
        {{--</ul>--}}
    {{--</li>--}}
</ul>
