@extends("master")

@section("page_header","Produk")
@section("page_description","Daftar Produk")
@push("contents")

    <div class="panel">
        <form class="panel-body" id="f-filter">
            <div class="col-sm-4">
                {!! Form::select("branch_id",$branches,null,["class"=>"form-control","placeholder"=>"Semua Gerai","id"=>"bid"]) !!}
            </div>

            <div class="col-sm-4">
                {!! Form::select("category_id",$categories,null,["class"=>"form-control","placeholder"=>"Semua Kategori","id"=>"cid"]) !!}
            </div>

            <div class="col-sm-4">
                <button type="submit" class="btn btn-primary">Filter</button>
                &nbsp;
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-product"><i class="fa fa-plus"></i> Tambah Produk</button>
            </div>
        </form>
    </div>

    <div class="panel">
        <div class="panel-body">
            <table class="table table-hover" id="dtable">
                <thead>
                <tr>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Harga Jual</th>
                    <th>Kategori</th>
                    <th>Gerai</th>
                    <th>Aksi</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endpush

@push("contents")
    <!-- Modal -->
    <div id="modal-product" class="modal fade" role="dialog">
        <form method="post" class="modal-dialog" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" name="id">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah/Edit Produk</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>

                    <div class="form-group">
                        <label for="base_price">Harga</label>
                        <input type="number" min="0" class="form-control" id="base_price" name="base_price" required>
                    </div>

                    <div class="form-group">
                        <label for="sell_price">Harga Jual</label>
                        <input type="number" min="0" class="form-control" id="sell_price" name="sell_price" required>
                    </div>

                    <div class="form-group">
                        <label for="category_id">Kategori</label>
                        {!! Form::select("category_id",$categories,null,["class"=>"form-control","required","id"=>"category_id"]) !!}
                    </div>

                    <div class="form-group">
                        <label for="img">Cover</label>
                        <input type="file" name="img" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="branch_id">Gerai</label>
                        {!! Form::select("branch_ids[]",$branches,null,["class"=>"form-control","required","id"=>"branch_id","multiple"]) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-default">Simpan</button>
                </div>
            </div>

        </form>
    </div>
@endpush

@push("scripts")
    <script>
        var dtable,dtableElem = $("#dtable");

        $(function(){
            dtable = dtableElem.DataTable({
                processing: true,
                serverSide: true,
                paging: false,
                searching:false,
                info:false,
                ajax: {
                    url: "{!! route('admin.product.data') !!}",
                    data: function(d){
                        d.branch_id = $("#bid").val();
                        d.category_id = $("#cid").val();
                    }
                },
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'base_price', name: 'base_price' },
                    { data: 'sell_price', name: 'sell_price' },
                    { data: 'category.display_name', name: 'category.display_name',orderable: false,searchable: false },
                    { data: 'branches', name: 'branches',orderable:false, searchable:false },
                    { data: 'action', name: 'action',orderable: false,searchable: false }
                ],
                order:[
                    [2,"desc"]
                ],
                columnDefs:[
                    {className:"text-right",targets:[1,2]},
                    {className:"text-center",targets:[3,5]}
                ]
            });
        });

        $(document).on("submit","#f-filter",function(e){
            e.preventDefault();
            dtable.ajax.reload();
        });


        $(document).on("show.bs.modal","#modal-product",function(e){
            var trig = $(e.relatedTarget),
                name = trig.data("name") || "",
                id = trig.data("id") || "",
                basePrice = trig.data("baseprice") || "",
                sellPrice = trig.data("sellprice") || "",
                categoryId = trig.data("cid") || "",
                branchIds = trig.data("bid") || "",
                modal = $(this);

            modal.find("input[name=name]").val(name);
            modal.find("input[name=id]").val(id);
            modal.find("input[name=base_price]").val(basePrice);
            modal.find("input[name=sell_price]").val(sellPrice);
            modal.find("select[name=category_id]").val(categoryId);

            if(branchIds !== ""){
                modal.find("select#branch_id").val(branchIds.length > 1 ? branchIds.split(","):[parseInt(branchIds)]);
            }

        });


    </script>
@endpush