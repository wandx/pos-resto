@extends("master")

@section("page_header","Kategori")
@section("page_description","Daftar Kategori")
@push("contents")
    <div class="row" style="margin-bottom: 1em;">
        <div class="col-sm-12 text-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modal-category">Tambah Kategori</button>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <table class="table table-hover dataTable" id="dtable">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Produk</th>
                        <th>Created</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
@endpush

@push("contents")
    <!-- Modal -->
    <div id="modal-category" class="modal fade" role="dialog">
        <form method="post" class="modal-dialog">
            {!! csrf_field() !!}
            <input type="hidden" name="id">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah/Edit Kategori</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" name="display_name" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-default">Simpan</button>
                </div>
            </div>

        </form>
    </div>
@endpush

@push("scripts")
    <script>
        var dtable,dtableElem = $("#dtable");

        $(function(){
            dtable = dtableElem.DataTable({
                processing: true,
                serverSide: true,
                paging: false,
                searching:false,
                info:false,
                ajax: '{!! route('admin.product.category.data') !!}',
                columns: [
                    { data: 'display_name', name: 'display_name' },
                    { data: 'product_count', name: 'product_count',orderable: false,searchable: false },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action',orderable: false,searchable: false }
                ],
                order:[
                    [2,"desc"]
                ],
                columnDefs:[
                    {className:"text-right",targets:[2]},
                    {className:"text-center",targets:[1,3]}
                ]
            });
        });


        $(document).on("show.bs.modal","#modal-category",function(e){
            var trig = $(e.relatedTarget),
                name = trig.data("name") || "",
                id = trig.data("id") || "",
                modal = $(this);

            modal.find("input[name=display_name]").val(name);
            modal.find("input[name=id]").val(id);
        });


    </script>
@endpush