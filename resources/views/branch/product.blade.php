@extends("master")

@section("page_header","Produk di Gerai ".$branch->title)
@section("page_description","Daftar Produk di Gerai ".$branch->title)
@push("contents")

    {{--<div class="panel">--}}
        {{--<form class="panel-body" id="f-filter">--}}
            {{--<div class="col-sm-4">--}}
                {{--{!! Form::select("branch_id",$branches,null,["class"=>"form-control","placeholder"=>"Semua Gerai","id"=>"bid"]) !!}--}}
            {{--</div>--}}

            {{--<div class="col-sm-4">--}}
                {{--{!! Form::select("category_id",$categories,null,["class"=>"form-control","placeholder"=>"Semua Kategori","id"=>"cid"]) !!}--}}
            {{--</div>--}}

            {{--<div class="col-sm-4">--}}
                {{--<button type="submit" class="btn btn-primary">Filter</button>--}}
                {{--&nbsp;--}}
                {{--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-product"><i class="fa fa-plus"></i> Tambah Produk</button>--}}
            {{--</div>--}}
        {{--</form>--}}
    {{--</div>--}}

    <div class="panel">
        <div class="panel-body">
            <table class="table table-hover" id="dtable">
                <thead>
                <tr>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Harga Jual</th>
                    <th>Kategori</th>
                    <th>Dengan Stok</th>
                    <th>Jumlah Stok</th>
                    <th>Aksi</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endpush

@push("contents")
    <!-- Modal -->
    <div id="modal-product" class="modal fade" role="dialog">
        <form method="post" action="{{ route("admin.branch.product_list.update_price") }}" class="modal-dialog modal-sm" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" name="id">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Produk</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="sell_price">Harga</label>
                        <input type="number" min="0" class="form-control" id="sell_price" name="sell_price" required>
                    </div>

                    <div class="form-group">
                        <label for="with-stock">Dengan Stock</label>
                        <select name="with_stock" class="form-control" required id="with-stock">
                            <option value="1">YA</option>
                            <option value="0">TIDAK</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="with-stock">Jumlah Stock</label>
                        <input type="number" class="form-control" id="stock" name="stock" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-default">Simpan</button>
                </div>
            </div>

        </form>
    </div>
@endpush

@push("scripts")
    <script>
        var dtable,dtableElem = $("#dtable");

        $(function(){
            dtable = dtableElem.DataTable({
                processing: true,
                serverSide: true,
                paging: false,
                searching:false,
                info:false,
                ajax: {
                    url: "{!! route('admin.branch.product_list.data',['branch_id'=>$branch->id]) !!}",
                },
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'base_price', name: 'base_price' },
                    { data: 'in_branch_price', name: 'in_branch_price', orderable: false,searchable: false },
                    { data: 'category.display_name', name: 'category.display_name',orderable: false,searchable: false },
                    { data: 'product_has_stock', name: 'product_has_stock',orderable: false,searchable: false },
                    { data: 'stock_count', name: 'stock_count',orderable: false,searchable: false },
                    { data: 'action', name: 'action',orderable: false,searchable: false }
                ],
                order:[
                    [0,"asc"]
                ],
                columnDefs:[
                    {className:"text-right",targets:[1,2]},
                    {className:"text-center",targets:[3,4,5,6]}
                ]
            });
        });

        $(document).on("submit","#f-filter",function(e){
            e.preventDefault();
            dtable.ajax.reload();
        });


        $(document).on("show.bs.modal","#modal-product",function(e){
            var trig = $(e.relatedTarget),
                name = trig.data("name") || "",
                id = trig.data("id") || "",
                sellPrice = trig.data("sellprice") || "",
                withStock = trig.data("withstock") || "0",
                stock = trig.data("stock") || "0",
                modal = $(this);

            modal.find("input[name=id]").val(id);
            modal.find("input[name=sell_price]").val(sellPrice);
            modal.find("select[name=with_stock]").val(withStock.toString());
            modal.find("input[name=stock]").val(stock);

        });


    </script>
@endpush