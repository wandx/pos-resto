@extends("master")

@section("page_header","Gerai")
@section("page_description","Daftar Gerai")
@push("contents")
    <div class="row" style="margin-bottom: 1em;">
        <div class="col-sm-12 text-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modal-branch">Tambah Gerai</button>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <table class="table table-hover dataTable" id="dtable">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Phone</th>
                        <th>Alamat</th>
                        <th>Produk</th>
                        <th>Pegawai</th>
                        <th>Created</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
@endpush

@push("contents")
    <!-- Modal -->
    <div id="modal-branch" class="modal fade" role="dialog">
        <form method="post" class="modal-dialog">
            {!! csrf_field() !!}
            <input type="hidden" name="id">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah/Edit Gerai</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Nama Gerai</label>
                        <input type="text" class="form-control" id="name" name="title" required>
                    </div>

                    <div class="form-group">
                        <label for="phone">No. Telfon</label>
                        <input type="text" class="form-control" id="phone" name="phone" required>
                    </div>

                    <div class="form-group">
                        <label for="address">Alamat</label>
                        <input type="text" class="form-control" id="address" name="address" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-default">Simpan</button>
                </div>
            </div>

        </form>
    </div>
@endpush

@push("scripts")
    <script>
        var dtable,dtableElem = $("#dtable");

        $(function(){
            dtable = dtableElem.DataTable({
                processing: true,
                serverSide: true,
                paging: false,
                searching:false,
                info:false,
                ajax: '{!! route('admin.branch.data') !!}',
                columns: [
                    { data: 'title', name: 'title' },
                    { data: 'phone', name: 'phone' },
                    { data: 'address', name: 'address' },
                    { data: 'product_count', name: 'product_count',orderable: false,searchable: false },
                    { data: 'user_count', name: 'user_count',orderable: false,searchable: false },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action',orderable: false,searchable: false }
                ],
                order:[
                    [5,"desc"]
                ],
                columnDefs:[
                    {className:"text-right",targets:[5]},
                    {className:"text-center",targets:[3,6,4]}
                ]
            });
        });


        $(document).on("show.bs.modal","#modal-branch",function(e){
            var trig = $(e.relatedTarget),
                name = trig.data("name") || "",
                id = trig.data("id") || "",
                address= trig.data("address") || "",
                phone = trig.data("phone") || "",
                modal = $(this);

            modal.find("input[name=title]").val(name);
            modal.find("input[name=id]").val(id);
            modal.find("input[name=address]").val(address);
            modal.find("input[name=phone]").val(phone);
        });


    </script>
@endpush