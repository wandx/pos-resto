<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchProductPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_product', function (Blueprint $table) {
            $table->uuid("bp_id");
            $table->unsignedInteger('branch_id')->index();
            $table->unsignedInteger("product_id")->index();
            $table->unsignedInteger("product_price")->default(0);
            $table->foreign("branch_id")->references("id")->on("branches")->onDelete("cascade");
            $table->foreign("product_id")->references("id")->on("products")->onDelete("cascade");

            $table->primary(["branch_id","product_id","bp_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_product');
    }
}
