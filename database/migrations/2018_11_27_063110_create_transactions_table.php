<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string("code",20);
            $table->string("customer_name");
            $table->unsignedInteger("cashier_id")->nullable();
            $table->unsignedInteger("kitchen_id")->nullable();
            $table->unsignedInteger("branch_id");
            $table->string("order_type",20)->default("dine_in");
            $table->string("order_status",20)->default("pending");
            $table->unsignedInteger("discount")->default(0);
            $table->boolean("is_discount_in_percent")->default(1);
            $table->string("discount_description")->nullable();
            $table->unsignedInteger("tax")->default(0);
            $table->unsignedInteger("grand_total")->default(0);
            $table->text("payment_data")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
