<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid("transaction_id");
            $table->unsignedInteger("product_id");
            $table->string("product_name",100);
            $table->string("notes")->nullable();
            $table->unsignedInteger("base_price");
            $table->unsignedInteger("sell_price");
            $table->unsignedInteger("quantity");
            $table->unsignedInteger("sub_total");
            $table->unsignedInteger("parent_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
