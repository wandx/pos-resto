<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            DB::table("admins")->insert([
                [
                    "name" => "admin",
                    "email" => "admin@saduk.com",
                    "username" => "admin",
                    "password" => bcrypt("password"),
                    "created_at" => \Carbon\Carbon::now(),
                    "updated_at" => \Carbon\Carbon::now(),
                ]
            ]);
        }catch (Exception $e){}
    }
}
